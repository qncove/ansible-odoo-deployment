**Ansible Odoo Deployment**
==============================

### Generating SSH Key

```

ssh-keygen

```

### Copying Key to remote server

```
ssh-copy-id root@remote_server.ip

```

### Install Ansible 1.7+

```
    sudo pip install ansible
```

### Running the Ansible Playbook

Simply run the Playbook with this command:
```
ansible-playbook -i remote_server_ip, -v deployment.yml 

```